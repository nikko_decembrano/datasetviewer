﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataSetViewerSampler
{
    public partial class Tester : Form
    {
        private DataSet SampleDataSet
        {
            get
            {
                DataSet dataSet = new DataSet("SampleDataSet");

                DataTable headerTable = dataSet.Tables.Add("HeaderTable");
                headerTable.Columns.Add("cus_no");
                headerTable.Columns.Add("cus_name");
                headerTable.Columns.Add("credit_limit");

                FillData(headerTable, "NIKDEC", "Nikko Decembrano", 10000);
                FillData(headerTable, "ANTVAS", "Anthony Vasquez", 5000);
                FillData(headerTable, "RODRIC", "Roderic Ricablanca", 5500);

                DataTable lineTable = dataSet.Tables.Add("LineTable");
                lineTable.Columns.Add("ord_no");
                lineTable.Columns.Add("cus_no");
                lineTable.Columns.Add("order_date");
                lineTable.Columns.Add("amount");

                FillData(lineTable, 1, "NIKDEC", "2018-01-01", 1000);
                FillData(lineTable, 2, "NIKDEC", "2018-01-02", 500);
                FillData(lineTable, 3, "ANTVAS", "2018-01-02", 600);
                FillData(lineTable, 4, "RODRIC", "2018-01-02", 760);
                FillData(lineTable, 5, "NIKDEC", "2018-01-02", 500);
                FillData(lineTable, 6, "ANTVAS", "2018-01-02", 599);
                FillData(lineTable, 7, "ANTVAS", "2018-01-02", 500);
                FillData(lineTable, 8, "NIKDEC", "2018-01-02", 100);
                FillData(lineTable, 9, "RODRIC", "2018-01-02", 500);
                FillData(lineTable, 10, "NIKDEC", "2018-01-02", 2000);

                return dataSet;
            }
        }

        public Tester()
        {
            InitializeComponent();
        }

        private void Tester_Load(object sender, EventArgs e)
        {
            dataSetViewerControl1.DataSet = SampleDataSet;
        }

        private void FillData(DataTable dataTable, params object[] values)
        {
            DataRow dataRow = dataTable.NewRow();
            for (int ctr = 0; ctr < values.Length; ctr++)
            {
                dataRow[ctr] = values[ctr];
            }
            dataTable.Rows.Add(dataRow);
        }
    }
}
