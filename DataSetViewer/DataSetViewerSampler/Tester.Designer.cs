﻿namespace DataSetViewerSampler
{
    partial class Tester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataSetViewerControl1 = new DataSetViewer.DataSetViewerControl();
            this.SuspendLayout();
            // 
            // dataSetViewerControl1
            // 
            this.dataSetViewerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataSetViewerControl1.Location = new System.Drawing.Point(0, 0);
            this.dataSetViewerControl1.Name = "dataSetViewerControl1";
            this.dataSetViewerControl1.Size = new System.Drawing.Size(800, 450);
            this.dataSetViewerControl1.TabIndex = 0;
            // 
            // Tester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataSetViewerControl1);
            this.Name = "Tester";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Tester_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private DataSetViewer.DataSetViewerControl dataSetViewerControl1;
    }
}

