﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataSetViewer
{
    public partial class DataSetViewerControl : UserControl
    {
        private DataSet DataSet_BackingField;

        public bool AllowExport = false;
        public bool ShowDebuggingBox = false;

        public DataSet DataSet
        {
            set
            {
                DataSet_BackingField = value;
                RefreshForm();
                RefreshDebugger();
            }
        }

        public DataSetViewerControl()
        {
            InitializeComponent();

            tsbQuickExport.Visible = AllowExport;
            tsbFullExport.Visible = AllowExport;
            groupDebugging.Visible = ShowDebuggingBox;

            toolStrip.Visible = toolStrip.Items.Cast<ToolStripItem>().Any(a => a.Visible);
        }

        private void RefreshForm()
        {
            tabControl.TabPages.Clear();
            foreach (DataTable dataTable in DataSet_BackingField.Tables)
            {
                TabPage tabPage = new TabPage(dataTable.TableName);
                var dataTableViewer = new DataTableViewerControl();
                tabPage.Controls.Add(dataTableViewer);
                dataTableViewer.Dock = DockStyle.Fill;
                dataTableViewer.DataTable = dataTable;
                tabControl.TabPages.Add(tabPage);
            }
        }

        private void RefreshDebugger()
        {
            if (DataSet_BackingField == null)
            {
                txtDebugger.Text = "No Data Set Passed";
            }
            else
            {
                txtDebugger.Text = "DataSetName: " + DataSet_BackingField.DataSetName + ", " + DataSet_BackingField.Tables.Count.ToString() + " Tables\r\n";
                foreach (DataTable table in DataSet_BackingField.Tables)
                {
                    txtDebugger.Text += string.Format("Table Name: {0}, {1} columns, {2} rows\r\n", table.TableName, table.Columns.Count, table.Rows.Count);
                }
            }
        }
    }
}
