﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DataSetViewer
{
    public partial class DataTableViewerControl : UserControl
    {
        private DataTable DataTable_BackingField;
        public DataTable DataTable
        {
            set
            {
                DataTable_BackingField = value;
                RefreshForm();
            }
        }

        public DataTableViewerControl()
        {
            InitializeComponent();
        }

        public void RefreshForm()
        {
            dataGridView.DataSource = DataTable_BackingField;
        }
    }
}
