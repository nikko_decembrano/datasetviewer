﻿namespace DataSetViewer
{
    partial class DataSetViewerControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataSetViewerControl));
            this.groupDebugging = new System.Windows.Forms.GroupBox();
            this.txtDebugger = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.tsbQuickExport = new System.Windows.Forms.ToolStripSplitButton();
            this.allSheetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbFullExport = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupDebugging.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupDebugging
            // 
            this.groupDebugging.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.groupDebugging.Controls.Add(this.txtDebugger);
            this.groupDebugging.Location = new System.Drawing.Point(690, 351);
            this.groupDebugging.Name = "groupDebugging";
            this.groupDebugging.Size = new System.Drawing.Size(276, 154);
            this.groupDebugging.TabIndex = 0;
            this.groupDebugging.TabStop = false;
            this.groupDebugging.Text = "Debugging Box";
            // 
            // txtDebugger
            // 
            this.txtDebugger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDebugger.Location = new System.Drawing.Point(3, 16);
            this.txtDebugger.Multiline = true;
            this.txtDebugger.Name = "txtDebugger";
            this.txtDebugger.ReadOnly = true;
            this.txtDebugger.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDebugger.Size = new System.Drawing.Size(270, 135);
            this.txtDebugger.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 25);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(987, 501);
            this.tabControl.TabIndex = 1;
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbQuickExport,
            this.tsbFullExport});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(987, 25);
            this.toolStrip.TabIndex = 2;
            this.toolStrip.Text = "toolStrip1";
            // 
            // tsbQuickExport
            // 
            this.tsbQuickExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbQuickExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allSheetsToolStripMenuItem,
            this.currentSheetToolStripMenuItem});
            this.tsbQuickExport.Image = ((System.Drawing.Image)(resources.GetObject("tsbQuickExport.Image")));
            this.tsbQuickExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbQuickExport.Name = "tsbQuickExport";
            this.tsbQuickExport.Size = new System.Drawing.Size(84, 22);
            this.tsbQuickExport.Text = "Quick Export";
            // 
            // allSheetsToolStripMenuItem
            // 
            this.allSheetsToolStripMenuItem.Name = "allSheetsToolStripMenuItem";
            this.allSheetsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.allSheetsToolStripMenuItem.Text = "All Sheet(s)";
            // 
            // currentSheetToolStripMenuItem
            // 
            this.currentSheetToolStripMenuItem.Name = "currentSheetToolStripMenuItem";
            this.currentSheetToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.currentSheetToolStripMenuItem.Text = "Current Sheet";
            // 
            // tsbFullExport
            // 
            this.tsbFullExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbFullExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.tsbFullExport.Image = ((System.Drawing.Image)(resources.GetObject("tsbFullExport.Image")));
            this.tsbFullExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFullExport.Name = "tsbFullExport";
            this.tsbFullExport.Size = new System.Drawing.Size(74, 22);
            this.tsbFullExport.Text = "Full Export";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem1.Text = "All Sheet(s)";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem2.Text = "Current Sheet";
            // 
            // DataSetViewerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupDebugging);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.toolStrip);
            this.Name = "DataSetViewerControl";
            this.Size = new System.Drawing.Size(987, 526);
            this.groupDebugging.ResumeLayout(false);
            this.groupDebugging.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupDebugging;
        private System.Windows.Forms.TextBox txtDebugger;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripSplitButton tsbQuickExport;
        private System.Windows.Forms.ToolStripMenuItem allSheetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currentSheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton tsbFullExport;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
    }
}
